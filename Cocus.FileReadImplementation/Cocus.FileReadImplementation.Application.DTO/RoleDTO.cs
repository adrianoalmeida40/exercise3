﻿namespace Cocus.FileReadImplementation.Application.DTO
{
    using System;

    public class RoleDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
