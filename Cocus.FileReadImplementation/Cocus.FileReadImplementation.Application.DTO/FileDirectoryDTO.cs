﻿namespace Cocus.FileReadImplementation.Application.DTO
{
    public class FileDirectoryDTO
    {
        public string FileType { get; set; }

        public string NameFile { get; set; }

        public string TextFile { get; set; }
    }
}
