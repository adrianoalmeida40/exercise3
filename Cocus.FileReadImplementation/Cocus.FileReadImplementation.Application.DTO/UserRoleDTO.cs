﻿namespace Cocus.FileReadImplementation.Application.DTO
{
    using System;

    public class UserRoleDTO
    {
        public Guid Id { get; set; }

        public Guid RoleId { get; set; }

        public string NameRole { get; set; }

        public Guid UserId { get; set; }
    }
}
