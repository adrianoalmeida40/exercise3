﻿namespace Cocus.FileReadImplementation.Application.DTO
{
    using System;

    public class UserDTO
    {
        public Guid Id { get; set; }

        public string Username { get; set; }
    }
}
