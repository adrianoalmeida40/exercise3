﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class FileDirectoryAdapter
    {
        public static FileDirectoryDTO ToDTO(this FileDirectory fileDirectory)
        {
            return new FileDirectoryDTO
            {
                FileType = fileDirectory.FileType,
                NameFile = fileDirectory.NameFile,
                TextFile = fileDirectory.TextFile
            };
        }

        public static IEnumerable<FileDirectoryDTO> ToDTO(this IEnumerable<FileDirectory> filesDirectory)
        {
            return filesDirectory.Select(fileDirectory => fileDirectory.ToDTO());
        }
    }
}
