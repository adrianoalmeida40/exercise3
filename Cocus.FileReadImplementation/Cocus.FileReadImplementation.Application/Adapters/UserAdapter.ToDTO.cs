﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class UserAdapter
    {
        public static UserDTO ToDTO(this User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                Username = user.Username
            };
        }

        public static IEnumerable<UserDTO> ToDTO(this IEnumerable<User> users)
        {
            return users.Select(user => user.ToDTO());
        }
    }
}
