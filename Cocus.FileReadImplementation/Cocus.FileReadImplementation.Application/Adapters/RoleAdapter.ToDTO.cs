﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class RoleAdapter
    {
        public static RoleDTO ToDTO(this Role role)
        {
            return new RoleDTO
            {
                Id = role.Id,
                Name = role.Name
            };
        }

        public static IEnumerable<RoleDTO> ToDTO(this IEnumerable<Role> roles)
        {
            return roles.Select(role => role.ToDTO());
        }
    }
}
