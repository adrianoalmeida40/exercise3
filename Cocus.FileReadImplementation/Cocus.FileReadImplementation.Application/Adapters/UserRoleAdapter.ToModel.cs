﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class UserRoleAdapter
    {
        public static UserRole ToModel(this UserRoleDTO userRoleDTO)
        {
            return new UserRole
            {
                Id = userRoleDTO.Id,
                NameRole = userRoleDTO.NameRole,
                RoleId = userRoleDTO.RoleId,
                UserId = userRoleDTO.UserId
            };
        }
    }
}
