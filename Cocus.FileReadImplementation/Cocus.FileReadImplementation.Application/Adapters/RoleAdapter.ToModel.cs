﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class RoleAdapter
    {
        public static Role ToModel(this RoleDTO roleDTO)
        {
            return new Role
            {
                Id = roleDTO.Id,
                Name = roleDTO.Name
            };
        }
    }
}
