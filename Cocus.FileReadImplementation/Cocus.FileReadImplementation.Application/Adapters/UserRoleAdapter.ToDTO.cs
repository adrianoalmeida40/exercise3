﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class UserRoleAdapter
    {
        public static UserRoleDTO ToDTO(this UserRole userRole)
        {
            return new UserRoleDTO
            {
                Id = userRole.Id,
                NameRole = userRole.NameRole,
                RoleId = userRole.RoleId,
                UserId = userRole.UserId
            };
        }

        public static IEnumerable<UserRoleDTO> ToDTO(this IEnumerable<UserRole> userRoles)
        {
            return userRoles.Select(userRole => userRole.ToDTO());
        }
    }
}
