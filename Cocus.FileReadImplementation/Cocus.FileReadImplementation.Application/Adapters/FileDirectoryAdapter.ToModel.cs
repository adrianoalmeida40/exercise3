﻿using Cocus.FileReadImplementation.Application.DTO;
using Cocus.FileReadImplementation.Domain.Models;

namespace Cocus.FileReadImplementation.Application.Adapters
{
    public static partial class FileDirectoryAdapter
    {
        public static FileDirectory ToModel(this FileDirectoryDTO fileDirectoryDTO)
        {
            return new FileDirectory
            {
                FileType = fileDirectoryDTO.FileType,
                NameFile = fileDirectoryDTO.NameFile,
                TextFile = fileDirectoryDTO.TextFile
            };
        }
    }
}
