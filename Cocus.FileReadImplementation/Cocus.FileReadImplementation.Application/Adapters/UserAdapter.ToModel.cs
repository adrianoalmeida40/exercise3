﻿namespace Cocus.FileReadImplementation.Application.Adapters
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Domain.Models;

    public static partial class UserAdapter
    {
        public static User ToModel(this UserDTO userDTO)
        {
            return new User
            {
                Id = userDTO.Id,
                Username = userDTO.Username
            };
        }
    }
}
