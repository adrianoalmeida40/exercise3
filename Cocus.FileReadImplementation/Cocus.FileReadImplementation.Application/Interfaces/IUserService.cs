﻿namespace Cocus.FileReadImplementation.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.DTO;

    public interface IUserService
    {
        Task<UserDTO> GetByIdAsync(Guid id);

        Task<IEnumerable<UserDTO>> GetAllAsync();

        Task<UserDTO> CreateAsync(UserDTO userDTO);

        Task<bool> UpdateAsync(UserDTO userDTO);

        Task<bool> DeleteAsync(Guid id);
    }
}
