﻿namespace Cocus.FileReadImplementation.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.DTO;

    public interface IRoleService
    {
        Task<RoleDTO> GetByIdAsync(Guid id);

        Task<IEnumerable<RoleDTO>> GetAllAsync();

        Task<RoleDTO> CreateAsync(RoleDTO roleDTO);
    }
}
