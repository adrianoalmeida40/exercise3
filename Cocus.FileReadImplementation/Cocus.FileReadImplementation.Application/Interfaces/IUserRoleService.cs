﻿namespace Cocus.FileReadImplementation.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.DTO;

    public interface IUserRoleService
    {
        Task<UserRoleDTO> GetByIdAsync(Guid id);

        Task<IEnumerable<UserRoleDTO>> GetAllAsync();

        Task<UserRoleDTO> CreateAsync(UserRoleDTO userRoleDTO);

        Task<bool> DeleteAsync(Guid id);
    }
}
