﻿namespace Cocus.FileReadImplementation.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.DTO;

    public interface IFileDirectoryService
    {
        FileDirectoryDTO GetByFileAsync(Guid userId, FileDirectoryDTO fileDirectoryDTO);

        IEnumerable<FileDirectoryDTO> GetAllAsync(Guid userId);

        Task<FileDirectoryDTO> CreateAsync(FileDirectoryDTO fileDirectoryDTO);

        Task<bool> UpdateAsync(FileDirectoryDTO fileDirectoryDTO);

        Task<bool> DeleteAsync(Guid id);
    }
}
