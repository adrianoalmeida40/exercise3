﻿namespace Cocus.FileReadImplementation.Application.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.Adapters;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Cocus.FileReadImplementation.Infra.Data.Repositories;

    public class FileDirectoryService : IFileDirectoryService
    {
        private readonly IFileDirectoryRepository fileDirectoryRepository;

        public FileDirectoryService(IFileDirectoryRepository fileDirectoryRepository)
        {
            this.fileDirectoryRepository = fileDirectoryRepository;
        }

        public Task<FileDirectoryDTO> CreateAsync(FileDirectoryDTO fileDirectoryDTO)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FileDirectoryDTO> GetAllAsync(Guid userId)
        {
            var files = fileDirectoryRepository.GetAllAsync(userId);

            return files.ToDTO();
        }

        public FileDirectoryDTO GetByFileAsync(Guid userId, FileDirectoryDTO fileDirectoryDTO)
        {
            var fileDirectory = fileDirectoryRepository.GetByFileAsync(userId, fileDirectoryDTO.ToModel());

            return fileDirectory.ToDTO();
        }

        public Task<bool> UpdateAsync(FileDirectoryDTO fileDirectoryDTO)
        {
            throw new NotImplementedException();
        }
    }
}
