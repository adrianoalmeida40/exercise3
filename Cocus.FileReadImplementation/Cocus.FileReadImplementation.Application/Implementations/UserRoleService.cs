﻿namespace Cocus.FileReadImplementation.Application.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.Adapters;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Cocus.FileReadImplementation.Infra.Data.Repositories;

    public class UserRoleService : IUserRoleService
    {
        private readonly IUserRoleRepository userRoleRepository;

        public UserRoleService(IUserRoleRepository userRoleRepository)
        {
            this.userRoleRepository = userRoleRepository;
        }

        public async Task<UserRoleDTO> CreateAsync(UserRoleDTO userRoleDTO)
        {
            var createUserRole = await userRoleRepository.CreateAsync(userRoleDTO.ToModel());

            return createUserRole.ToDTO();
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            return await userRoleRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<UserRoleDTO>> GetAllAsync()
        {
            var userRoles = await this.userRoleRepository.GetAllAsync();

            return userRoles?.ToDTO();
        }

        public async Task<UserRoleDTO> GetByIdAsync(Guid id)
        {
            var userRole = await userRoleRepository.GetByIdAsync(id).ConfigureAwait(false);

            return userRole?.ToDTO();
        }
    }
}
