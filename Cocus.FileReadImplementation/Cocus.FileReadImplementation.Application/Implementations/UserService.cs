﻿namespace Cocus.FileReadImplementation.Application.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.Adapters;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Cocus.FileReadImplementation.Infra.Data.Repositories;

    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<UserDTO> CreateAsync(UserDTO userDTO)
        {
            var createUser = await userRepository.CreateAsync(userDTO.ToModel());

            return createUser.ToDTO();
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            return await userRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await this.userRepository.GetAllAsync();

            return users?.ToDTO();
        }

        public async Task<UserDTO> GetByIdAsync(Guid id)
        {
            var user = await userRepository.GetByIdAsync(id).ConfigureAwait(false);

            return user?.ToDTO();
        }

        public Task<bool> UpdateAsync(UserDTO userDTO)
        {
            return userRepository.UpdateAsync(userDTO.ToModel());
        }
    }
}
