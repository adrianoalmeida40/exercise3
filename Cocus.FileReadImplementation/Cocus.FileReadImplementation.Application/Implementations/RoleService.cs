﻿namespace Cocus.FileReadImplementation.Application.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Application.Adapters;
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Cocus.FileReadImplementation.Infra.Data.Repositories;

    public class RoleService : IRoleService
    {
        private readonly IRoleRepository roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            this.roleRepository = roleRepository;
        }

        public async Task<RoleDTO> CreateAsync(RoleDTO roleDTO)
        {
            var createRole = await roleRepository.CreateAsync(roleDTO.ToModel());

            return createRole.ToDTO();
        }

        public async Task<IEnumerable<RoleDTO>> GetAllAsync()
        {
            var userRoles = await this.roleRepository.GetAllAsync();

            return userRoles?.ToDTO();
        }

        public async Task<RoleDTO> GetByIdAsync(Guid id)
        {
            var userRole = await roleRepository.GetByIdAsync(id).ConfigureAwait(false);

            return userRole?.ToDTO();
        }
    }
}
