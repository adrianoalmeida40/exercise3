﻿namespace Cocus.FileReadImplementation.Domain.Models
{
    using System;
    using MongoDB.Bson.Serialization.Attributes;

    public class Role
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
