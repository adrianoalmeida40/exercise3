﻿namespace Cocus.FileReadImplementation.Domain.Models
{
    using System;
    using MongoDB.Bson.Serialization.Attributes;

    public class UserRole
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid RoleId { get; set; }

        public string NameRole { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid UserId { get; set; }
    }
}
