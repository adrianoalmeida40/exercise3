﻿namespace Cocus.FileReadImplementation.Domain.Models
{
    public class FileDirectory
    {
        public string FileType { get; set; }

        public string NameFile { get; set; }

        public string TextFile { get; set; }
    }
}
