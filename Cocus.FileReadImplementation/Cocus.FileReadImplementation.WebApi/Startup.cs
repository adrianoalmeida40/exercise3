﻿namespace Cocus.FileReadImplementation.WebApi
{
    using Cocus.FileReadImplementation.Crosscutting.Configuration;
    using Cocus.FileReadImplementation.WebApi.Setup;
    using Cocus.ReadFileImplementation.Crosscutting.IoC;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.SwaggerUI;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IConfiguration configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var applicationSettings = this.configuration.GetSection(nameof(ApplicationSettings)).Get<ApplicationSettings>();
            services.AddSingleton<IApplicationSettings>(applicationSettings);

            services
                .AddMongoDb(applicationSettings)
                .AddFileDirectory(applicationSettings)
                .AddSwaggerGen(s =>
                {
                    s.SwaggerDoc("v1",
                        new Swashbuckle.AspNetCore.Swagger.Info { Title = "File Read Implementation", Version = "V1" });
                    s.DescribeAllEnumsAsStrings();
                })
                .AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationSettings applicationSettings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app
                .UseMongoDb(applicationSettings)
                .UseSwagger()
                .UseSwaggerUI(config =>
                {
                    config.SwaggerEndpoint("/swagger/v1/swagger.json", "File Read Implementation V1");
                    config.DocExpansion(DocExpansion.None);
                })
                .UseMvc();
        }

        private static void RegisterServices(IServiceCollection services)
        {
            // Adding dependencies from another layers (isolated from Presentation)
            IoC.RegisterServices(services);
        }

        private void OnApplicationStopped(IApplicationBuilder app)
        {
            app.StopMongoDb();
        }
    }
}
