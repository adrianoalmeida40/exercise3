﻿namespace Cocus.FileReadImplementation.WebApi.Setup
{
    using System;
    using Cocus.FileReadImplementation.Crosscutting.Configuration;
    using Cocus.FileReadImplementation.Infra.Data.Connection;
    using Cocus.FileReadImplementation.Infra.Data.Connection.Implementation;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;

    public static class MongoDbSetup
    {
        public static IServiceCollection AddMongoDb(this IServiceCollection services, IApplicationSettings applicationSettings)
        {
            var mongoConnection = MongoDbConnection.BuildFrom(applicationSettings.Mongo.FileReadImplementation);

            return services.AddSingleton<IMongoDbConnection>(mongoConnection);
        }

        public static IApplicationBuilder UseMongoDb(this IApplicationBuilder app, IApplicationSettings applicationSettings)
        {
            try
            {
                var mongoDbConnection = app.ApplicationServices.GetRequiredService<IMongoDbConnection>();

                mongoDbConnection.Connect();

                return app;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static IApplicationBuilder StopMongoDb(this IApplicationBuilder app)
        {
            try
            {
                var mongoDbConnection = app.ApplicationServices.GetRequiredService<IMongoDbConnection>();

                mongoDbConnection.Shutdown();

                return app;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
