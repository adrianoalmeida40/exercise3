﻿using Cocus.FileReadImplementation.Crosscutting.Configuration;
using Cocus.FileReadImplementation.Infra.Data.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Cocus.FileReadImplementation.WebApi.Setup
{
    public static class FileDirectorySetup
    {
        public static IServiceCollection AddFileDirectory(this IServiceCollection services, IApplicationSettings applicationSettings)
        {
            var fileTextRead = new TxtFileReadImplementation(applicationSettings);

            return services.AddSingleton<ITxtFileReadImplementation>(fileTextRead);
        }
    }
}
