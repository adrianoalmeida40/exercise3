﻿namespace Cocus.FileReadImplementation.WebApi.Controllers
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;

    [Route("v1/[controller]")]
    public class FileDirectoryController : Controller
    {
        private readonly IFileDirectoryService fileDirectoryService;

        public FileDirectoryController(IFileDirectoryService fileDirectoryService)
        {
            this.fileDirectoryService = fileDirectoryService;
        }

        [HttpGet("userId")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<FileDirectoryDTO>))]
        public IActionResult GetAllAsync([FromQuery] Guid userId)
        {
            var filesDirectory = fileDirectoryService.GetAllAsync(userId);

            if (filesDirectory == null)
            {
                return this.Ok(new List<FileDirectoryDTO>());
            }

            return this.Ok(filesDirectory);
        }

        [HttpPost]
        [Route("GetByFileAsync")]
        [ProducesResponseType(200, Type = typeof(FileDirectoryDTO))]
        [ProducesResponseType(404)]
        public IActionResult GetByFileAsync([FromQuery] Guid userId,
                                            [FromBody] FileDirectoryDTO fileDirectoryDTO)
        {
            var fileDirectory = fileDirectoryService.GetByFileAsync(userId, fileDirectoryDTO);

            if (fileDirectory == null)
            {
                return this.NotFound();
            }

            return this.Ok(fileDirectory);
        }
    }
}