﻿namespace Cocus.FileReadImplementation.WebApi.Controllers
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    [Route("v1/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(UserDTO))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var userDTO = await userService.GetByIdAsync(id).ConfigureAwait(false);

            if (userDTO == null)
            {
                return this.NotFound();
            }

            return this.Ok(userDTO);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync([FromBody] UserDTO userDTO)
        {
            var createdUserDto = await userService.CreateAsync(userDTO).ConfigureAwait(false);

            return this.Created(createdUserDto.Id.ToString(), createdUserDto);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateAsync([FromRoute] Guid id, [FromBody] UserDTO userDTO)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.NotFound();
            }

            userDTO.Id = id;

            await userService.UpdateAsync(userDTO).ConfigureAwait(false);

            return this.Ok();
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<UserDTO>))]
        public async Task<IActionResult> GetAllAsync()
        {
            var usersDTO = await userService.GetAllAsync().ConfigureAwait(false);

            if (usersDTO == null)
            {
                return this.Ok(new List<UserDTO>());
            }

            return this.Ok(usersDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.BadRequest("The Id cannot be empty");
            }

            await userService.DeleteAsync(id).ConfigureAwait(false);

            return this.Ok();
        }
    }
}