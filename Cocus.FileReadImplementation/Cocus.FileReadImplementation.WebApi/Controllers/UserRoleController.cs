﻿namespace Cocus.FileReadImplementation.WebApi.Controllers
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    [Route("v1/[controller]")]
    public class UserRoleController : Controller
    {
        private readonly IUserRoleService userRoleService;

        public UserRoleController(IUserRoleService userRoleService)
        {
            this.userRoleService = userRoleService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(UserRoleDTO))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var userRoleDTO = await userRoleService.GetByIdAsync(id).ConfigureAwait(false);

            if (userRoleDTO == null)
            {
                return this.NotFound();
            }

            return this.Ok(userRoleDTO);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync([FromBody] UserRoleDTO userRoleDTO)
        {
            var createdUserRoleDto = await userRoleService.CreateAsync(userRoleDTO).ConfigureAwait(false);

            return this.Created(createdUserRoleDto.Id.ToString(), createdUserRoleDto);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<UserRoleDTO>))]
        public async Task<IActionResult> GetAllAsync()
        {
            var userRolesDTO = await userRoleService.GetAllAsync().ConfigureAwait(false);

            if (userRolesDTO == null)
            {
                return this.Ok(new List<UserRoleDTO>());
            }

            return this.Ok(userRolesDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.BadRequest("The Id cannot be empty");
            }

            await userRoleService.DeleteAsync(id).ConfigureAwait(false);

            return this.Ok();
        }
    }
}