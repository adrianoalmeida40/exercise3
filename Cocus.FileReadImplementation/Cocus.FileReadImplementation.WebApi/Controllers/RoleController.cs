﻿namespace Cocus.FileReadImplementation.WebApi.Controllers
{
    using Cocus.FileReadImplementation.Application.DTO;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    [Route("v1/[controller]")]
    public class RoleController : Controller
    {
        private readonly IRoleService roleService;

        public RoleController(IRoleService roleService)
        {
            this.roleService = roleService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(RoleDTO))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var roleDTO = await roleService.GetByIdAsync(id).ConfigureAwait(false);

            if (roleDTO == null)
            {
                return this.NotFound();
            }

            return this.Ok(roleDTO);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync([FromBody] RoleDTO roleDTO)
        {
            var createdRoleDto = await roleService.CreateAsync(roleDTO).ConfigureAwait(false);

            return this.Created(createdRoleDto.Id.ToString(), createdRoleDto);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<RoleDTO>))]
        public async Task<IActionResult> GetAllAsync()
        {
            var userRolesDTO = await roleService.GetAllAsync().ConfigureAwait(false);

            if (userRolesDTO == null)
            {
                return this.Ok(new List<RoleDTO>());
            }

            return this.Ok(userRolesDTO);
        }
    }
}