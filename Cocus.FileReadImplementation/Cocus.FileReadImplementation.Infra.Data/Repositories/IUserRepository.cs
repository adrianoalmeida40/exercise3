﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;

    public interface IUserRepository
    {
        Task<User> GetByIdAsync(Guid id);

        Task<IEnumerable<User>> GetAllAsync();

        Task<User> CreateAsync(User user);

        Task<bool> UpdateAsync(User user);

        Task<bool> DeleteAsync(Guid id);
    }
}
