﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;

    public interface IFileDirectoryRepository
    {
        FileDirectory GetByFileAsync(Guid userId, FileDirectory fileDirectory);

        IEnumerable<FileDirectory> GetAllAsync(Guid userId);

        Task<FileDirectory> CreateAsync(FileDirectory fileDirectory);

        Task<bool> UpdateAsync(FileDirectory fileDirectory);

        Task<bool> DeleteAsync(Guid id);
    }
}
