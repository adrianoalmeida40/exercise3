﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;
    using Cocus.FileReadImplementation.Infra.Data.Connection;
    using MongoDB.Driver;

    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IMongoDbConnection mongoDbConnection)
            : base(mongoDbConnection, "user")
        {

        }

        public async Task<User> CreateAsync(User user)
        {
            user.Id = Guid.NewGuid();

            await this.GetCollection().InsertOneAsync(user).ConfigureAwait(false);

            return user;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<User>.Filter.Eq(user => user.Id, id);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await this.GetCollection().Find(_ => true).ToListAsync().ConfigureAwait(false);
        }

        public async Task<User> GetByIdAsync(Guid id)
        {
            var filter = Builders<User>.Filter.Eq(user => user.Id, id);

            var result = await this.GetCollection().FindAsync(filter).ConfigureAwait(false);

            return await result.SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<bool> UpdateAsync(User user)
        {
            var filter = Builders<User>.Filter.Eq(userModel => userModel.Id, user.Id);

            var updateBuilder = Builders<User>.Update
                                    .Set(userUpdate => userUpdate.Username, user.Username);

            var result = await this.GetCollection().UpdateOneAsync(filter, updateBuilder).ConfigureAwait(false);

            return result.ModifiedCount > 0;
        }
    }
}
