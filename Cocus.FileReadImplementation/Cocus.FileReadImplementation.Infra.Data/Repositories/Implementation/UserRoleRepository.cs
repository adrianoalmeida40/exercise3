﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;
    using Cocus.FileReadImplementation.Infra.Data.Connection;
    using MongoDB.Driver;

    public class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(IMongoDbConnection mongoDbConnection)
            : base(mongoDbConnection, "userRole")
        {

            
        }

        public async Task<UserRole> CreateAsync(UserRole userRole)
        {
            userRole.Id = Guid.NewGuid();

            await this.GetCollection().InsertOneAsync(userRole).ConfigureAwait(false);

            return userRole;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<UserRole>.Filter.Eq(userRole => userRole.Id, id);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<IEnumerable<UserRole>> GetAllAsync()
        {
            return await this.GetCollection().Find(_ => true).ToListAsync().ConfigureAwait(false);
        }

        public async Task<UserRole> GetByIdAsync(Guid id)
        {
            var filter = Builders<UserRole>.Filter.Eq(userRole => userRole.Id, id);

            var result = await this.GetCollection().FindAsync(filter).ConfigureAwait(false);

            return await result.SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public IEnumerable<UserRole> GetByUser(Guid userId)
        {
            var filter = Builders<UserRole>.Filter.Eq(userRole => userRole.UserId, userId);

            var result = this.GetCollection().Find(filter);

            return result.ToList();
        }
    }
}
