﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories.Implementation
{
    using Cocus.FileReadImplementation.Infra.Data.Connection;
    using MongoDB.Driver;

    public class BaseRepository<TEntity> where TEntity : class
    {
        protected readonly IMongoDbConnection mongoDbConnection;
        private readonly string collectionName;

        public BaseRepository(IMongoDbConnection mongoDbConnection, string collectionName)
        {
            this.mongoDbConnection = mongoDbConnection;
            this.collectionName = collectionName;
        }

        protected IMongoCollection<TEntity> GetCollection()
        {
            return this.mongoDbConnection.GetCollection<TEntity>(collectionName);
        }
    }
}
