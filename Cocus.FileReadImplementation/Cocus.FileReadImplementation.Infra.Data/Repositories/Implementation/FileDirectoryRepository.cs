﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;
    using Cocus.FileReadImplementation.Infra.Data.Interfaces;

    public class FileDirectoryRepository : IFileDirectoryRepository
    {
        private readonly ITxtFileReadImplementation txtFileReadImplementation;
        private readonly IUserRoleRepository userRoleRepository;

        public FileDirectoryRepository(ITxtFileReadImplementation txtFileReadImplementation,
                                       IUserRoleRepository userRoleRepository)
        {
            this.txtFileReadImplementation = txtFileReadImplementation;
            this.userRoleRepository = userRoleRepository;
        }

        public Task<FileDirectory> CreateAsync(FileDirectory fileDirectory)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FileDirectory> GetAllAsync(Guid userId)
        {
            List<FileDirectory> fileDirectories = new List<FileDirectory>();
            string[] filePaths = Directory.GetFiles(txtFileReadImplementation.TxtFileRead.TxtFileRead);

            var userRoles = userRoleRepository.GetByUser(userId).ToList();

            foreach(var filePath in filePaths)
            {
                if(userRoles.Exists(userRole => userRole.NameRole == Path.GetExtension(filePath)))
                {
                    FileDirectory fileDirectory = new FileDirectory
                    {
                        FileType = Path.GetExtension(filePath),
                        NameFile = filePath
                    };

                    fileDirectories.Add(fileDirectory);
                }                
            }

            return fileDirectories;
        }

        public FileDirectory GetByFileAsync(Guid userId, FileDirectory fileDirectory)
        {
            string text = System.IO.File.ReadAllText(fileDirectory.NameFile);

            fileDirectory.TextFile = text;

            return fileDirectory;
        }

        public Task<bool> UpdateAsync(FileDirectory fileDirectory)
        {
            throw new NotImplementedException();
        }
    }
}
