﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;
    using Cocus.FileReadImplementation.Infra.Data.Connection;
    using MongoDB.Driver;

    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(IMongoDbConnection mongoDbConnection)
            : base(mongoDbConnection, "role")
        {

        }

        public async Task<Role> CreateAsync(Role role)
        {
            role.Id = Guid.NewGuid();

            await this.GetCollection().InsertOneAsync(role).ConfigureAwait(false);

            return role;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<Role>.Filter.Eq(role => role.Id, id);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<IEnumerable<Role>> GetAllAsync()
        {
            return await this.GetCollection().Find(_ => true).ToListAsync().ConfigureAwait(false);
        }

        public async Task<Role> GetByIdAsync(Guid id)
        {
            var filter = Builders<Role>.Filter.Eq(user => user.Id, id);

            var result = await this.GetCollection().FindAsync(filter).ConfigureAwait(false);

            return await result.SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<bool> UpdateAsync(Role role)
        {
            var filter = Builders<Role>.Filter.Eq(roleModel => roleModel.Id, role.Id);

            var updateBuilder = Builders<Role>.Update
                                    .Set(roleUpdate => roleUpdate.Name, role.Name);

            var result = await this.GetCollection().UpdateOneAsync(filter, updateBuilder).ConfigureAwait(false);

            return result.ModifiedCount > 0;
        }
    }
}
