﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;

    public interface IUserRoleRepository
    {
        Task<UserRole> GetByIdAsync(Guid id);

        Task<IEnumerable<UserRole>> GetAllAsync();

        IEnumerable<UserRole> GetByUser(Guid userId);

        Task<UserRole> CreateAsync(UserRole userRole);

        Task<bool> DeleteAsync(Guid id);
    }
}
