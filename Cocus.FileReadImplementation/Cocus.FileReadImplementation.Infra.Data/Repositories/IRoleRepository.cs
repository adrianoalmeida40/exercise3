﻿namespace Cocus.FileReadImplementation.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FileReadImplementation.Domain.Models;

    public interface IRoleRepository
    {
        Task<Role> GetByIdAsync(Guid id);

        Task<IEnumerable<Role>> GetAllAsync();

        Task<Role> CreateAsync(Role role);

        Task<bool> UpdateAsync(Role role);

        Task<bool> DeleteAsync(Guid id);
    }
}
