﻿namespace Cocus.FileReadImplementation.Infra.Data.Configuration
{
    public class MongoSettings
    {
        public string ConnectionString { get; set; }
    }
}
