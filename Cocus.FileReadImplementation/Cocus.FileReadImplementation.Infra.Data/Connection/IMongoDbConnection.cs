﻿namespace Cocus.FileReadImplementation.Infra.Data.Connection
{
    using MongoDB.Driver;

    public interface IMongoDbConnection
    {
        void Connect();

        void Shutdown();

        IMongoCollection<T> GetCollection<T>(string collectionName);
    }
}
