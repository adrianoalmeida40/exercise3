﻿namespace Cocus.FileReadImplementation.Infra.Data.Connection.Implementation
{
    using System;
    using System.Net.Sockets;
    using Cocus.FileReadImplementation.Infra.Data.Configuration;
    using MongoDB.Driver;    

    public sealed class MongoDbConnection : IMongoDbConnection
    {
        private readonly MongoSettings configuration;
        private MongoClient mongoClient;
        private MongoUrl mongoUrl;

        public MongoDbConnection(MongoSettings configuration)
        {
            this.configuration = configuration;
        }

        public static IMongoDbConnection BuildFrom(MongoSettings configuration)
        {
            return new MongoDbConnection(configuration ?? throw new ArgumentNullException(nameof(configuration)));
        }

        public void Connect()
        {
            this.mongoUrl = new MongoUrl(this.configuration.ConnectionString);

            var settings = MongoClientSettings.FromUrl(this.mongoUrl);

            settings.MaxConnectionIdleTime = TimeSpan.FromSeconds(30);

            settings.ClusterConfigurator = builder => builder.ConfigureTcp(tcp => tcp.With(socketConfigurator: (Action<Socket>)SocketConfigurator));

            this.mongoClient = new MongoClient(settings);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            var database = this.mongoClient.GetDatabase(this.mongoUrl.DatabaseName);
            return database.GetCollection<T>(collectionName);
        }

        public void Shutdown()
        {
            this.mongoUrl = null;
            this.mongoClient = null;
        }

        private void SocketConfigurator(Socket s) => s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
    }
}
