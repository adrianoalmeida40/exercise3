﻿namespace Cocus.FileReadImplementation.Infra.Data.Interfaces
{
    using Cocus.FileReadImplementation.Infra.Data.Configuration;

    public interface ITxtFileReadImplementation
    {
        ReadImplementationSettings TxtFileRead { get; }
    }
}
