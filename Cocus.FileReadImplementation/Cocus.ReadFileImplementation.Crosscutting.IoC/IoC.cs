﻿namespace Cocus.ReadFileImplementation.Crosscutting.IoC
{
    using Cocus.FileReadImplementation.Application.Implementations;
    using Cocus.FileReadImplementation.Application.Interfaces;
    using Cocus.FileReadImplementation.Crosscutting.Configuration;
    using Cocus.FileReadImplementation.Infra.Data.Interfaces;
    using Cocus.FileReadImplementation.Infra.Data.Repositories;
    using Cocus.FileReadImplementation.Infra.Data.Repositories.Implementation;
    using Microsoft.Extensions.DependencyInjection;

    public class IoC
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ITxtFileReadImplementation, TxtFileReadImplementation>();

            // Infra - Data
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IFileDirectoryRepository, FileDirectoryRepository>();

            //Service
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRoleService, UserRoleService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IFileDirectoryService, FileDirectoryService>();
        }
    }
}
