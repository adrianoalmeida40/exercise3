﻿namespace Cocus.FileReadImplementation.Crosscutting.Configuration
{
    public interface IApplicationSettings
    {
        MongoConnections Mongo { get; }

        TxtFileReadImplementation TxtFileRead { get; }
    }
}
