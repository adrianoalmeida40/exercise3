﻿namespace Cocus.FileReadImplementation.Crosscutting.Configuration
{
    using Cocus.FileReadImplementation.Infra.Data.Configuration;
    using Cocus.FileReadImplementation.Infra.Data.Interfaces;

    public class TxtFileReadImplementation : ITxtFileReadImplementation
    {
        public TxtFileReadImplementation(IApplicationSettings applicationSettings)
        {
            this.TxtFileRead = applicationSettings.TxtFileRead.TxtFileRead;
        }

        public TxtFileReadImplementation() { }

        public ReadImplementationSettings TxtFileRead { get; set; }
    }
}
