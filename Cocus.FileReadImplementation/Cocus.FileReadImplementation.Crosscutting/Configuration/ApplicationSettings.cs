﻿namespace Cocus.FileReadImplementation.Crosscutting.Configuration
{
    public class ApplicationSettings : IApplicationSettings
    {
        public MongoConnections Mongo { get; set; }

        public TxtFileReadImplementation TxtFileRead { get; set; }
    }
}
